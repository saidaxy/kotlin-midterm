package com.example.midtermsaida.di

import androidx.room.Room
import com.example.lab3.databaseProject.data.CatViewModel
import com.example.midtermsaida.data.CatRepository
import com.example.midtermsaida.db.AppDatabase
import com.example.midtermsaida.db.dao.CatDao
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.lang.reflect.Array.get

val dbModule = module {
    single { Room.databaseBuilder(androidContext(),
        AppDatabase::class.java, AppDatabase.DB_NAME).build() }

    single { get<AppDatabase>().getCatDao() }
}

val repoModule = module {
    single { CatRepository(get() as CatDao) }
}

val viewModelModule = module {
    viewModel { CatViewModel(get() as CatRepository) }
    viewModel { CatViewModel(get() as CatRepository) }
}