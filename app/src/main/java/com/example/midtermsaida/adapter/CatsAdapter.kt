package com.example.midtermsaida.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.midtermsaida.R
import com.example.midtermsaida.models.Cats

class CatsAdapter(private val cats: List<Cats>) : RecyclerView.Adapter<CatsAdapter.CatViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_item_cat, parent, false)
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        holder.bindCat(cats[position])
    }

    override fun getItemCount() = cats.size

    inner class CatViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindCat(cat: Cats) {
        }


    }
}