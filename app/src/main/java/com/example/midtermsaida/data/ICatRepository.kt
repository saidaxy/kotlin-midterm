package com.example.midtermsaida.data

import com.example.midtermsaida.models.Cat
import com.example.midtermsaida.models.Cats

interface ICatRepository {

    suspend fun insertCats(cat : Cat)

    suspend fun getAllCats() : List<Cats>
}
