package com.example.midtermsaida.data

import com.example.midtermsaida.db.dao.CatDao
import com.example.midtermsaida.models.Cat
import com.example.midtermsaida.models.Cats
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import kotlinx.coroutines.withContext

class CatRepository(private val catDao: CatDao): ICatRepository {

    override suspend fun insertCats(cat: Cat) {
        return withContext((Dispatchers.IO){
            catDao.insertCats(cat)
        }

    }
    override suspend fun getAllCats(): List<Cats> {
        return withContext(Dispatchers.IO) {
            catDao.getAllCats()
        }
    }
}