package com.example.lab3.databaseProject.data


import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.midtermsaida.data.ICatRepository
import com.example.midtermsaida.models.Cat

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CatViewModel(
    private val repository: ICatRepository
) : ViewModel(), CoroutineScope {
    private val mutableCatLD = MutableLiveData<List<Cat>>()
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job
    val catLD: MutableLiveData<List<Cat>>
        get() = mutableCatLD


    /*fun loadCat() {
        launch {
            Log.d("taaag")
            mutableCatLD.postValue(repository.getAllCats())
        }
    }*/

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    class Factory(private val repository: ICatRepository) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CatViewModel::class.java)) {
                return CatViewModel(repository) as T
            }
            throw IllegalStateException("meow")
        }
    }
}

