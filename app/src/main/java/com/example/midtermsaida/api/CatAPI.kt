package com.example.midtermsaida.api

import com.example.midtermsaida.models.Cat
import retrofit2.http.GET

interface CatApi {
    @GET("facts")
    suspend fun getAllCats(): Cat
}