package com.example.midtermsaida.utils

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.lab3.databaseProject.data.CatViewModel
import com.example.midtermsaida.R
import com.example.midtermsaida.api.CatService.retrofit
import com.example.midtermsaida.models.Cats
import org.koin.android.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private val catViewModel: CatViewModel by viewModel()



    fun ApiCallAndPutInDB(){

        val catAPI = retrofit.create<CatAPI>(CatAPI::class.java)


        CatAPI.getAllCats().enqueue(object : Callback<List<Cats>> {

            override fun onFailure(call: Call<List<CatAPI>>?, t: Throwable?) {
                Log.e(TAG,"OOPS!! something went wrong..")
            }

            override fun onResponse(call: Call<List<Cats>>?, response: Response<List<Cats>>?) {

                Log.e(TAG,response!!.body().toString())
                when(response.code())
                {
                    200 ->{
                        Thread(Runnable {

                        }).start()
                    }
                }

            }
        })
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
