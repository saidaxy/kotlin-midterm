package com.example.midtermsaida.models

data class Cat(
    val all: List<Cats>
)

data class Cats(
    val _id: String,
    val text: String,
    val type: String,
    val upvotes: Int,
    val userUpvoted: Any
)



