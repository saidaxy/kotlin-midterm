package com.example.midtermsaida.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.midtermsaida.models.Cat
import com.example.midtermsaida.models.Cats


@Dao

interface CatDao{

    @Query("SELECT * FROM catsfacts")
    fun getAllCats(): List<Cats>

    @Insert
    fun insertCats(cat: Cat)
}