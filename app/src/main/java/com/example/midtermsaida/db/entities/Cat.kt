package com.example.midtermsaida.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "catsfacts")
data class Cat(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    @ColumnInfo(name = "text") val text: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "upvotes") val upvotes: Int,
    @ColumnInfo(name = "userUpvoted") val userUpvoted: Any)