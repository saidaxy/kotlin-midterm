package com.example.midtermsaida.db
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.midtermsaida.db.dao.CatDao
import com.example.midtermsaida.models.Cats

@Database(entities = [Cats::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getCatDao(): CatDao

    companion object {

        const val DB_NAME = "cats.db"

        var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase? {
            if (instance == null) {
                synchronized(AppDatabase::class.java) {
                    instance = Room.databaseBuilder(context,
                        AppDatabase::class.java,
                        DB_NAME
                    ).build()
                }
            }

            return instance
        }
    }
}
